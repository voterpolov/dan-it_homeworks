"use strict";

// let maxCount = 0;

// for (let i = 10; i > maxCount; i = i - 3) {
//    console.log("hi");
//    //    console.log(i);
// }

// let i = 0;

// while (i < 1) {
//    console.log(i);
//    i += 2;
// }

// do {
//    while (i < 4) {
//       console.log(i); // 0, 1, 2, 3
//       i++;
//    }

//    console.log(i); // 4
//    i += 2;
// } while (i < 0);

// console.log(i); // 6

// for (let i = 0; i < 10; i++) {
//    if (i === 4) {
//       console.log(i);
//       // break;
//       //   continue;
//    } else {
//       console.log(`continue`);
//       continue;
//    }
//    console.log(`step end`);
// }

// TASK 1
// for (let i = 0; i <= 300; ++i) {
//    if (i % 2 !== 0) {
//       if (i % 5 !== 0) {
//          console.log(i);
//       }
//    }
// }

// TASK 2
// let firstNumber;
// do {
//    firstNumber = prompt("Enter the first number");
// } while (
//    !firstNumber ||
//    typeof +firstNumber !== "number" ||
//    isNaN(+firstNumber) ||
//    firstNumber.trim() === ""
// );
// !firstNumber -> !null -> firstNumber === null

// let secondNumber;
// do {
//    secondNumber = prompt("Enter the second number");
// } while (
//    !secondNumber ||
//    typeof +secondNumber !== "number" ||
//    isNaN(+secondNumber) ||
//    secondNumber.trim() === ""
// );

// console.log(
//    `Поздравляем. Введённые вами числа: «${firstNumber}» и «${secondNumber}».`
// );

let userName;
let userLastName;
let userYearsOfBirth;

do {
   userName = prompt("Укажи своё имя");
} while (!userName || userName.trim() === "");

do {
   userLastName = prompt("Укажи свою фамилию");
} while (!userLastName || userLastName.trim() === "");

do {
   userYearsOfBirth = prompt("Укажи свой год рождения");
} while (
   !userYearsOfBirth ||
   typeof +userYearsOfBirth !== "number" ||
   isNaN(+userYearsOfBirth) ||
   userYearsOfBirth.trim() === "" ||
   userYearsOfBirth < 1910 ||
   userYearsOfBirth > 2020
);

console.log(
   `«Добро пожаловать, родившийся в ${userYearsOfBirth} , ${userName} ${userLastName}.»`
);
