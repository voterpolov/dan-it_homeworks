"use strict";

// console.log(summ(1, 2));
// function summ(a, b) {
//    // some code...
//    console.log("function complete");
//    return a + b;
// }

// const summing = function (a, b) {
//    // some code...
//    console.log("function complete");
//    return a + b;
// };
// console.log(summing(3, 4));

// const ArrowFnSum = (a, b) => a + b;
// const ArrowFnSum = (a = 0, b = 0, ...args) => {
//    console.log(args);
//    console.log(...args);
//    console.log(a, b);
//    return a + b;
// };
// console.log(ArrowFnSum(2, 3, 4, 5, 6, 7, 8, 9, 0));

// const user = {
//    name: "Roman",
//    getName: function () {
//       console.log(this.name);
//    },
//    arrow: {
//       name: "Arrow",
//       getName: () => {
//          console.log(this);
//       },
//    },
// };

// console.log({ age: 33, ...user });

// user.getName.call(user.arrow);
// user.arrow.getName.call(user);

// const sum = (a) => {
//    return (b) => {
//       return (c) => {
//          return a + b + c;
//       };
//    };
// };

// console.log(sum(1)(2)(3));

// const sumOneTwoAnd = sum(1)(2);
// console.log(sumOneTwoAnd(5));
// console.log(sumOneTwoAnd(15));
// console.log(sumOneTwoAnd(25));

// const summingFn = (num) => {
//    let res = 0;
//    for (let index = 0; index < num; index++) {
//       res += index;
//    }
//    return res;
// };

// const sum = (a, callbackFn) => {
//    return callbackFn(a);
// };

// console.log(sum(10, summingFn));

// const checkNum = (a) => (a % 2 === 0 ? "Even" : "Odd");
// // const fn = checkNum;
// const checkRangeValue = (a, b, callbackFn) => {
//    for (let i = a; i < b; i++) {
//       console.log(i, callbackFn(i));
//    }
// };
// checkRangeValue(1, 30, checkNum);

// const checkOneValue = (a, callbackFn) => {
//    console.log(a, callbackFn(a));
// };
// checkOneValue(5, checkNum);

// function ask(question, yes, no) {
//    if (confirm(question)) yes();
//    else no();
// }

// ask(
//    "Вы согласны?",
//    function () {
//       alert("Вы согласились.");
//    },
//    function () {
//       alert("Вы отменили выполнение.");
//    }
// );

// TASK 1
// const sum = (a, b) => a + b;

// const sum = (a, b) => {
//    return a + b;
// };

// TASK 2

const sum = (...rest) => {
   if (rest.length < 2) {
      return "Error";
   }
   for (let i = 0; i < rest.length; i++) {
      if (typeof rest[i] !== "number" || isNaN(+rest[i])) {
         return `Error ${i + 1}`;
      }
   }
   let res = 0;
   for (let i of rest) {
      res += i;
   }
   return res;
};
// console.log(sum(3, 5, 6, 8));
// Task 3

// const maxValue = (...rest) => {
//    if (rest.length < 2) {
//       return "Error";
//    }
//    for (let i = 0; i < rest.length; i++) {
//       if (typeof rest[i] !== "number" || isNaN(+rest[i])) {
//          return `Error ${i + 1}`;
//       }
//    }
//    return Math.max(...rest);
// };
// console.log(maxValue(3, 5, 6, 8));

// TASK 4
// const logger = (str = "«Внимание! Сообщение не указано.»", num = 1) => {
//    for (let i = 0; i < num; i++) {
//       console.log(str);
//    }
// };

// logger("Smth", 4);

// TASK 5
// const add = (a, b) => a + b;
// const subtract = (a, b) => a - b;
// const multiply = (a, b) => a * b;
// const divide = (a, b) => a / b;

// const calculate = (a, b, callback) => callback(a, b);

// console.log(calculate(5, 7, divide));

// for (const key in "sdfghjkl;") {
//    console.log(key, "sdfghjkl;"[key]);
// }
