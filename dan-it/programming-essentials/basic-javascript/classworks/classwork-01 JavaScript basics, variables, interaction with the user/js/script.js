"use strict";

// let MAX_AGE = 101;
// alert("hi from inside");
/*
here
is 
comments
*/
// var a;
// 3;
// alert(a);
// alert(3);

// let a = 3;
// const b = 4;

// a = 4;
// b = 5;

// const userName = "Roman";
// const userLastName = "Marchenko";

// console.log(window);
// console.dir(window);

// var b = 3;

// const myNumber = 33.33;
// // console.log(myNumber);
// console.log(typeof myNumber); // number
// // console.log(typeof typeof myNumber);

// const myNaN = NaN;
// // console.log(typeof myNaN); // number

// const userName = "Roman";
// // console.log(userName);
// console.log(typeof userName); // string

// const userAge = "33";
// // console.log(typeof userAge); // string

// const variableType = "string";

// const myBoolean = true; // true || false, type -> boolean
// console.log(typeof myBoolean);

// const variableNull = null;
// console.log(typeof variableNull); // object

// const variableUNdef = undefined;
// console.log(typeof variableUNdef); // undefined

// const variableObject = { name: "Roman", age: 18 };
// console.log(typeof variableObject); // object

// const variableArray = [
//    [1, 2, 3],
//    2,
//    3,
//    "Roman",
//    { name: "Roman", age: 18 },
//    variableObject,
// ];
// console.log(typeof variableArray); // object

// function fnName() {}
// console.log(typeof fnName); // function
// "";
// const userAnswer = prompt("Write your answer");
// console.log(userAnswer, typeof userAnswer);

// const userConfirm = confirm("Are you 18 years old?");
// console.log(userConfirm, typeof userConfirm);

// console.log(typeof typeof 3);
// console.log(typeof "number");

// let b = "123";
// // b = 6;
// console.log(typeof b);
// b = parseInt(b);
// console.log(typeof b);
// b = parseFloat(b);
// console.log(typeof b);
// b = +b;
// console.log(typeof b);
// alert("Hello! This is alert");
// const userAnswer = prompt("Enter the number");
// console.log(userAnswer);

// const firstNumber = prompt("Write your first number");
// const secondNumber = prompt("Write your second number");

// console.log(+firstNumber + +secondNumber);
// console.log(+firstNumber - +secondNumber);
// console.log(+firstNumber * +secondNumber);
// console.log(+firstNumber / +secondNumber);
// console.log(+firstNumber % +secondNumber);
