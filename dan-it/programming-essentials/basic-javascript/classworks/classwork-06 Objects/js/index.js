"use strict";

// const title = "Acer ...";
// const title2 = "Lenovo ...";
// const oldPrice = 22999;
// const newPrice = 19999;
// const topRated = true;

// // const obj = {
// //    title,
// //    title2,
// //    oldPrice,
// //    newPrice,
// //    topRated,
// // };

// // console.log(obj);

// // 1 var - global constructor
// // const product = new Object();

// // 2 var - fucntion constructor

// // 3 var - object literal
// const product = {
//    title: "Acer ...",
//    _oldPrice: 22999,

//    set oldPrice(value) {
//       if (typeof value !== "number" || value < 0) {
//          console.log("Error");
//          return;
//       }
//       this._oldPrice = value;
//    },
//    get oldPrice() {
//       return this._oldPrice;
//    },

//    "new price": 19999,
//    "top-rated": true,
//    discount: 0.15,
//    getProductTitle: function () {
//       return this.title;
//    },
//    getPriceWithDiscount() {
//       return this["new price"] * this.discount;
//    },
// };

// product.oldPrice = "30000";

// console.log(product.oldPrice);
// // console.log(product._oldPrice);

// product.count = 33;
// // console.log(product.title);
// // console.log(product.getProductTitle());
// // console.log(product.getPriceWithDiscount());

// // const product2 = product;

// const product2 = {};

// for (const key in product) {
//    //    console.log(key);
//    product2[key] = product[key];
// }

// // const product2 = Object.assign({}, product);

// // console.log(product);
// console.log(product2);

// product2.count = 10;
// // console.log(product.count);
// // console.log(product2.count);

// product2.title = "Mac Air";

// // console.log(`${product2.getProductTitle}`);
// // console.log(`${product2.getProductTitle()}`);
// // const obj1 = { a: 6 };
// // const obj2 = { b: 3 };

// // console.log(Object.assign(obj1, obj2));

// // console.log(Object.keys(product));
// // console.log(Object.entries(product));
// // console.log(Object.values(product));

// Object.defineProperty(product2, "oldPrice", {
//    set: function (value) {
//       if (typeof value !== "number" || value < 0) {
//          console.log("Error");
//          return;
//       }
//       this._oldPrice = value;
//    },
//    get: function () {
//       return this._oldPrice;
//    },
// });

// product2.oldPrice = -3;
// console.log(product2.oldPrice);

// TASK 1

const user = {
   name: "Uasya",
   surname: "Pupkin",
   profesion: "Gruzchik",
   sayHi: function () {
      console.log(`Привет. Меня зовут ${this.name} ${this.surname}.`);
   },
   changePropertyValue(property, value) {
      if (property in this) {
         this[property] = value;
      } else {
         console.log("Error");
      }
   },
   addNewProperty(property, value) {
      if (property in this) {
         console.log("Error");
      } else {
         this[property] = value;
      }
   },
};
user.sayHi();

user.changePropertyValue("name", "sfg");
console.log(user["name"]);
console.log(user.property);

for (const key in user) {
   // user[key]
   console.log(`${key} -${user[key]} `);
}
