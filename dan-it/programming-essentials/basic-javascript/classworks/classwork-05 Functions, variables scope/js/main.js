"use strict";

// IIFE
// (function () {})();
// "Hi";
// const str = "Hi";

// const num1 = "4";
// const num2 = 3;
// let result;

// console.log(sum(num1, num2));

// function sum(a, b) {
//    let result = a + b;
//    return result;
// }

// result;

// const sum2 = function (a, b) {
//    console.log(arguments);
//    console.log(arguments.length);
//    let result = a + b;
//    return result;
// };
// console.log(sum2(2, 3));

// sum2 = "";
// sum2();

// console.log(typeof sum);
// console.log(sum instanceof Object);

// function recursionCreate(value, value2, value3, value4, value5) {
//    console.log(arguments);
//    console.log(arguments.length);

//    for (let index = 0; index < arguments.length; index++) {
//       console.log(arguments[index]);
//    }

//    console.log(value);
//    value++;
//    if (value < 30) {
//       recursionCreate(value);
//    }
// }

// recursionCreate("first", null, "third", "fourth");

// function recursionCreate() {
//    console.log("rec2");
// }
// recursionCreate();

// const rec2 = recursionCreate;
// rec2();

// TASK 1

// const num1 = 25;
// const num2 = 20;

// function sumNumber(num1, num2) {
//    return num1 + num2;
// }
// console.log(sumNumber(num1, num2));

// TASK 2
// function lengthFun(num1, num2) {
//    return arguments.length;
//    console.log(arguments.length);
// }
// console.log(lengthFun(12, 3, 4, 5));

// TASK 3
// function count(num1, num2) {
//    if (num2 < num1) {
//       console.log("«⛔️ Ошибка! Счёт невозможен.»");
//    } else if (num1 === num2) {
//       console.log(" «⛔️ Ошибка! Нечего считать.»");
//    } else {
//       console.log("«🏁 Отсчёт начат.");
//       for (let i = num1; i <= num2; i++) {
//          console.log(i);
//       }
//       console.log(" «✅ Отсчёт завершен.».");
//    }
// }

// count(6, 5);

// TASK 4
// const counter = function (num1, num2, num3) {
//    if (
//       typeof num1 === "number" &&
//       typeof num2 === "number" &&
//       typeof num3 === "number" &&
//       num1 !== null &&
//       num2 !== null &&
//       num3 !== null &&
//       !isNaN(+num1) &&
//       !isNaN(+num2) &&
//       !isNaN(+num3)
//    ) {
//       if (num2 < num1) {
//          console.log("«⛔️ Ошибка! Счёт невозможен.»");
//       } else if (num1 === num2) {
//          console.log(" «⛔️ Ошибка! Нечего считать.»");
//       } else {
//          console.log("«🏁 Отсчёт начат.");
//          for (let i = num1; i <= num2; i++) {
//             if (i % num3 === 0) {
//                console.log(i);
//             }
//          }
//          console.log(" «✅ Отсчёт завершен.».");
//       }
//    } else if (arguments.length < 3) {
//       console.log("type more numbers");
//    } else {
//       console.log("type numbers");
//    }
// };
// counter(6, 4, 3);

// TASK 5

function sum() {
   if (arguments.length <= 2) {
      console.log("елементов меньше двух");
      return;
   }
   for (let index = 0; index < arguments.length; index++)
      if (
         typeof arguments[index] === "number" &&
         arguments[index] !== null &&
         !isNaN(+arguments[index])
      ) {
         console.log("OK");
         // your code here ...
      } else {
         console.log("ошибка в елементе номера " + (index + 1));
      }
   console.log(arguments.length);
}
sum(4, 2, 3);
