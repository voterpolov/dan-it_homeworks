"use strict";

// let userAge = "-18";

// if (userAge < 0) {
//    console.log("Its working");
// } else {
//    console.log("Its not working");
// }

// if (userAge < 0) {
//    console.log("userAge < 0");
// } else if (userAge === 0) {
//    console.log("userAge === 0");
// } else {
//    console.log("userAge > 0");
// }

// if(){} else if(){} else {}

// if (typeof userAge === "string") {
//    userAge = +userAge;
//    if (userAge < 0) {
//       console.log("userAge < 0");
//    } else if (userAge === 0) {
//       console.log("userAge === 0");
//    } else {
//       console.log("userAge > 0");
//    }
// } else {
//    console.log("type of userAge is not 'string'");
// }

// const userChoise = prompt("Make your choise: 1, 2, 3");

// switch (userChoise) {
//    case "1":
//       console.log(`Your number is ${userChoise}`);
//       break;

//    case "2":
//       console.log(`Your number is ${userChoise}`);
//       break;

//    case "3":
//       console.log(`Your number is ${userChoise}`);
//       break;

//    default:
//       console.log(`Your default number is ${userChoise}`);
//       break;
// }

// TASK 1if (!isNaN(+number) && number !== "" && number !== null)
// let number = prompt("Enter the number:");
// if (!isNaN(+number) && number !== "" && number !== null) {
//    if (number % 2 === 0) {
//       console.log("Ваше число чётное.");
//    } else {
//       console.log("Ваше число нечётное.");
//    }
// } else {
//    number = prompt("Enter the number:");
//     {
//       if (number % 2 === 0) {
//          console.log("Ваше число чётное.");
//       } else {
//          console.log("Ваше число нечётное.");
//       }
//    } else {
//       alert("⛔️ Ошибка! Вы ввели не число.");
//    }
// }

// TASK 2
// const userName = prompt("Ваше имя: ");
// if (userName === "Mike") {
//    console.log("Привет Майк, ты - CEO");
// } else if (userName === "Jane") {
//    console.log("Привет Джейн, ты - CTO");
// } else if (userName === "Walter") {
//    console.log("Привет Волтер, ты - программист");
// } else if (userName === "Oliver") {
//    console.log("Привет Оливер, ты - менеджер");
// } else if (userName === "John") {
//    console.log("Привет Джон, ты - уборщик");
// } else {
//    console.log("Пользователь не найден.");
// }

// switch (userName) {
//    case "Mike":
//       console.log(`Привет Майк, ты - CEO`);
//       break;

//    case "Jane":
//       console.log("Привет Джейн, ты - CTO");
//       break;

//    case "Walter":
//       console.log("Привет Волтер, ты - программист");
//       break;

//    case "Oliver":
//       console.log("Привет Оливер, ты - менеджер");
//       break;

//    case "John":
//       console.log("Привет Джон, ты - уборщик");
//       break;

//    default:
//       console.log("Пользователь не найден.");
//       break;
// }

// TASK 3
// let firstNumber = prompt("Write a number");
// let secondNumber = prompt("Write second number");
// let thirdNumber = prompt("Write third number");
// if (
//    !isNaN(+firstNumber) &&
//    firstNumber !== "" &&
//    firstNumber !== null &&
//    !isNaN(+secondNumber) &&
//    secondNumber !== "" &&
//    secondNumber !== null &&
//    !isNaN(+thirdNumber) &&
//    thirdNumber !== "" &&
//    thirdNumber !== null
// ) {
//    if (firstNumber > secondNumber && firstNumber > thirdNumber) {
//       console.log(firstNumber);
//    } else if (secondNumber > firstNumber && secondNumber > thirdNumber) {
//       console.log(secondNumber);
//    } else {
//       console.log(thirdNumber);
//    }
// } else {
//    console.log("⛔️ Ошибка! Одно из введённых чисел не является числом.");
// }

// TASK 4
// const drink = prompt("Выберите напиток; Кофе-25, Чай-10, Капучино-50");
// const money = prompt("Внесите сумму");
// let change;
// const TEA_COST = 10;
// const COFFE_COST = 25;
// const CAPPUCHINO_COST = 50;

// switch (drink) {
//    case "Кофе":
//       change = money - COFFE_COST;
//       break;
//    case "Чай":
//       change = money - TEA_COST;
//       break;
//    case "Капучино":
//       change = money - CAPPUCHINO_COST;
//       break;
//    default:
//       console.log(`Wrong drink!! Take you money back: ${money}`);
//       break;
// }

// if (change && change === 0) {
//    console.log(`Ваш «${drink}» готов. Спасибо за сумму без сдачи! :)`);
// }
// if (change && change < 0) {
//    console.log(`Добавте денег ${-change}`);
// }
// if (change && change > 0) {
//    console.log(`Ваш «${drink}» готов. Возьмите сдачу: ${change}`);
// }

// if (drink === "Кофе") {
//    let change = +money - 25;
//    if (change === 0) {
//       console.log("Ваш «Кофе» готов. Спасибо за сумму без сдачи! :)");
//    } else if (change < 0) {
//       console.log("Добавте денег " + -change);
//    } else {
//       console.log("Ваш «Кофе» готов. Возьмите сдачу: " + change);
//    }
// }
// if (drink === "Чай") {
//    let change = +money - 10;
//    if (change === 0) {
//       console.log("Ваш «Чай» готов. Спасибо за сумму без сдачи! :)");
//    } else if (change < 0) {
//       console.log("Добавте денег " + -change);
//    } else {
//       console.log("Ваш «Чай» готов. Возьмите сдачу: " + change);
//    }
// }
// if (drink === "Капучино") {
//    let change = +money - 50;
//    if (change === 0) {
//       console.log("Ваш «Капучино» готов. Спасибо за сумму без сдачи! :)");
//    } else if (change < 0) {
//       console.log("Добавте денег " + -change);
//    } else {
//       console.log("Ваш «Капучино» готов. Возьмите сдачу: " + change);
//    }
// }

// const age = 18;
// let parentAgree = false;

// // if (age < 10) {
// //    parentAgree = !parentAgree;
// // }
// parentAgree = age < 10 ? !parentAgree : name ? name : "Uasya";
// console.log(parentAgree);

// let adminStatus = false;
// const userName = prompt("What is your name") || "Admin";
// console.log(userName);

// if (
//    userName === "Uasya" ||
//    userName === "Petro" ||
//    userName === "Max" ||
//    userName === "Olha"
// ) {
//    adminStatus = true;
// }

// adminStatus =
//    userName === "Uasya" ||
//    userName === "Petro" ||
//    userName === "Max" ||
//    userName === "Olha"
//       ? true
//       : false;

// console.log(adminStatus);

// if (true){/*code*/}else if (true){/*code*/}else{/*code*/}

var a = 2;
if (true) {
   var a = 5;
   console.log(a);
}
console.log(a);
//////////
let b = 2;
if (true) {
   let b = 5;
   console.log(b);
}
console.log(b);
