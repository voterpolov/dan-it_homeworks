function mathAction(num1, num2, customOper) {
    do {
        num1 = prompt('Введите первое число', num1);
        num2 = prompt('Введите второе число', num2);
    } while (
        !num1 ||
        typeof +num1 !== 'number' ||
        isNaN(+num1) ||
        num1.trim() === '' ||
        !num2 ||
        typeof +num2 !== 'number' ||
        isNaN(+num2) ||
        num2.trim() === ''
    );
    customOper = prompt('Какую операцию произвести?');

    if (customOper == '*') {
        return console.log(+num1 * +num2);
    } else if (customOper == '/') {
        return console.log(+num1 / +num2);
    } else if (customOper == '+') {
        return console.log(+num1 + +num2);
    } else if (customOper == '-') {
        return console.log(+num1 - +num2);
    }
}

mathAction();
