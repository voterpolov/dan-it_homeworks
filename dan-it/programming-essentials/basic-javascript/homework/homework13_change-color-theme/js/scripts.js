const styleBtn = document.getElementById('styleBtn');
const wrapperDiv = document.getElementById('wrapper');

window.onload = function () {
    wrapperDiv.style.background = localStorage.getItem('colorOfBackground');
};

function changeColor() {
    if (localStorage.getItem('colorOfBackground') === 'green') {
        localStorage.setItem('colorOfBackground', 'orchid');
        wrapperDiv.style.background = localStorage.getItem('colorOfBackground');
    } else {
        localStorage.setItem('colorOfBackground', 'green');
        wrapperDiv.style.background = localStorage.getItem('colorOfBackground');
    }

    // wrapperDiv.style.background = 'green';
    // localStorage.setItem('colorOfBackground', 'green');
}

styleBtn.addEventListener('click', changeColor);
