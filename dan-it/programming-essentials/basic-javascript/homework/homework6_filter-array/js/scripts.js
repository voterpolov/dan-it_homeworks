function filterBy(arr, type) {
    const newArr = arr.filter((arg) => typeof arg !== type);
    return newArr;
}

console.log(filterBy(['hello', 'world', 23, '23', null, '123123'], 'string'));
console.log(filterBy(['hello', 'world', 23, '23', null, '123123'], 'number'));
