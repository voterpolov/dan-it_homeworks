'use strict';
const button = document.createElement('button');
button.innerText = 'Draw circle';
document.body.prepend(button);

const wrapper = document.createElement('div');
wrapper.style.display = 'grid';
wrapper.style.gridTemplateColumns = '1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr';

const input = document.createElement('input');
button.addEventListener('click', addInput);

function addInput() {
    document.body.prepend(input);
    input.placeholder = 'Diameter of circles? (px)';
    button.innerText = 'Draw!';
    button.addEventListener('click', drawCirles);
    function drawCirles() {
        document.body.prepend(wrapper);
        for (let i = 0; i < 100; i++) {
            const circle = document.createElement('div');
            circle.className = 'circle';
            circle.style.borderRadius = '50%';
            circle.style.width = input.value + 'px';
            circle.style.height = input.value + 'px';
            circle.style.backgroundColor = getRandomColor();
            wrapper.prepend(circle);
            function getRandomColor() {
                const r = Math.ceil(Math.random() * 255);
                const g = Math.ceil(Math.random() * 255);
                const b = Math.ceil(Math.random() * 255);

                return `rgb(${r}, ${g}, ${b})`;
            }
        }
        input.remove();
        button.remove();
    }
    wrapper.onclick = function (event) {
        if (event.target.classList.contains('circle')) {
            let target = event.target;
            target.remove();
        }
    };
}
