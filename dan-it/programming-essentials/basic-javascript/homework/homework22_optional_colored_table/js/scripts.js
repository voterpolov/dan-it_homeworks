'use strict';

function createTable(widthTable, heightTable) {
    const table = document.createElement('table');
    const body = document.body;
    for (let i = 0; i < widthTable; i++) {
        const tr = document.createElement('tr');
        table.append(tr);
        for (let i = 0; i < heightTable; i++) {
            const td = document.createElement('td');
            table.append(td);
            td.style.border = '1px dotted green';
            td.style.width = '20px';
            td.style.height = '20px';
            td.style.backgroundColor = 'white';
        }
    }
    document.body.prepend(table);
    body.addEventListener('click', reversColors);
    function reversColors(e) {
        if (e.target.tagName === 'TD') {
            if (e.target.style.backgroundColor === 'black') {
                e.target.style.backgroundColor = 'white';
            } else {
                e.target.style.backgroundColor = 'black';
            }
        } else if (e.target.tagName === 'BODY') {
            const allTd = document.getElementsByTagName('td');
            Array.from(allTd).forEach((el) => {
                if (el.style.backgroundColor === 'white') {
                    el.style.backgroundColor = 'black';
                } else {
                    el.style.backgroundColor = 'white';
                }
            });
        }
    }
}

createTable(30, 30);
