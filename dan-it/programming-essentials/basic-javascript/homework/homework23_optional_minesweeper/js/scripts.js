'use strict';

const wrapper = document.createElement('div');
const widthField = 8; // prompt
const heightField = 8; // prompt
const amountOfCells = widthField * heightField;
const amountOfBombs = 10; // (widthField*heightField/6)
wrapper.style.display = 'grid';
wrapper.style.gridTemplateColumns = `repeat(${widthField}, 20px)`;
wrapper.innerHTML = '<button></button>'.repeat(amountOfCells);
document.body.prepend(wrapper);
document.querySelectorAll('button').forEach((el) => (el.style.height = '20px'));

// function idBomb (row, column) {
//     let index = row * WIDTH + column
// }
