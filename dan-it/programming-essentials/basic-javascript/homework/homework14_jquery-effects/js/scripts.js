// button toggle
$('#toggleButton').click(function () {
    $('#toggleMe').slideToggle('slow');
});
$('.jQuery').css('color', 'orange');

// animated scroll
$('.jQuery').click(function (event) {
    event.preventDefault();
    let clickId = event.target.id;
    let top = $(`.${clickId}`).offset().top;
    $('html, body').animate(
        {
            scrollTop: top,
        },
        1000
    );
});

//button UP

$(window).scroll(function () {
    if ($(window).scrollTop() > $(window).height()) $('#top').fadeIn();
    else $('#top').fadeOut();
});
$('#top').click(function () {
    $('body, html').animate(
        {
            scrollTop: 0,
        },
        500
    );
});
