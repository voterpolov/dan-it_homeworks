'use strict';

function main(elements, parent = document.body) {
    const container = document.createElement('div');
    container.innerHTML = parseElement(elements);
    parent.prepend(container);

    const total = 3;
    const timeMachine = document.createElement('h2');
    const updateTime = (seconds) => {
        if (seconds > 0) {
            timeMachine.innerText = `${seconds} sec`;
        } else {
            container.remove();
        }
    };
    updateTime(total);
    container.prepend(timeMachine);

    countdown(total, 1, updateTime);
}

function parseElement(element) {
    if (Array.isArray(element)) {
        return `<ul>${element
            .map(parseElement)
            .map((el) => `<li>${el}</li>`)
            .join('')}</ul>`;
    } else {
        return element;
    }
}

function countdown(total, interval, cb) {
    setTimeout(() => {
        const timeLeft = total - interval;
        cb(timeLeft);
        if (timeLeft > 0) countdown(timeLeft, interval, cb);
    }, interval * 1000);
}

const myOptionalArr = ['Kharkiv', 'Kiev', ['Borispol', 'Irpin'], 'Odessa', 'Lviv', 'Dnieper'];
const myOptionalArr2 = [
    '1',
    '2',
    '3',
    ['1a', '2a', ['11', '22', 'wordword'], 'word'],
    'sea',
    'user',
    23,
    ['lastArr', 'withTwoArgs'],
];

// main(myOptionalArr);
// main(myOptionalArr2);
