'use strict';
function arrToLi(arr, parent = document.body) {
    const unorderedList = document.createElement('ul');
    parent.prepend(unorderedList);
    unorderedList.innerHTML = arr.map((element) => `<li>${element}</li>`).join('');
}
const myArr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const myArr2 = ['1', '2', '3', 'sea', 'user', 23];
// arrToLi(myArr);
// arrToLi(myArr2);
