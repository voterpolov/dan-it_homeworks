'use strict';
function tabSwitcher() {
    const tabsContent = document.querySelectorAll('ul.tabs-content > li');
    const tabsTitle = document.querySelectorAll('ul.tabs > li');
    let tabTitle = document.querySelectorAll('.tabs-title ');
    let tabContent = document.querySelectorAll('ul.tabs-content > li');

    addClassNumber(tabsContent);
    function addClassNumber(element) {
        let i = 0;
        element.forEach((el) => {
            el.classList.add('tab', `tab-${i + 1}`);
            i++;
        });
    }

    addDataAttr(tabsTitle);
    function addDataAttr(element) {
        let i = 0;
        element.forEach((el) => {
            el.dataset.tabPosition = `tab-${i + 1}`;
            i++;
        });
    }

    tabTitle.forEach((el) => {
        el.addEventListener('click', pickedTabTitle);
    });

    function pickedTabTitle() {
        tabTitle.forEach((el) => {
            el.classList.remove('active');
        });
        this.classList.add('active');
        let activeTab = this.getAttribute('data-tab-position');
        showTabTitleContent(activeTab);
    }

    function showTabTitleContent(activeTab) {
        tabContent.forEach((el) => {
            if (el.classList.contains(activeTab)) {
                el.classList.add('active');
            } else {
                el.classList.remove('active');
            }
        });
    }
}

tabSwitcher();
