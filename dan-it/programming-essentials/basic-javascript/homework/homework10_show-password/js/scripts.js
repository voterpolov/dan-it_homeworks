'use strict';
const eye = document.querySelectorAll('.fa-eye');
const input = document.querySelectorAll('input');
const confirmBtn = document.getElementsByTagName('button');
const attention = document.createElement('p');
attention.style.marginTop = 0;

countInputs();
function countInputs() {
    let i = 1;
    input.forEach((el) => {
        el.id = `input${i}`;
        i++;
        el.addEventListener('focus', removeAttention);
    });
}
function removeAttention() {
    attention.innerText = '';
}

eye.forEach((el) => {
    el.addEventListener('click', showHide);
});

function showHide(e) {
    if (this.classList.contains('fa-eye')) {
        this.classList.add('fa-eye-slash');
        this.classList.remove('fa-eye');
        this.previousSibling.previousSibling.type = 'text';
    } else {
        this.classList.add('fa-eye');
        this.classList.remove('fa-eye-slash');
        this.previousSibling.previousSibling.type = 'password';
    }
}

confirmBtn[0].onclick = function checkAndStop(event) {
    if (input1.value !== input2.value) {
        attention.innerText = 'You must write identical passwords';
        input2.parentNode.insertBefore(attention, input2.nextSibling);
        event.preventDefault();
    } else if (input1.value === '' || input2.value === '') {
        attention.innerText = 'enter the passwords';
        input2.parentNode.insertBefore(attention, input2.nextSibling);
        event.preventDefault();
    } else {
        alert('You r welcome!');
        event.preventDefault();
    }
};
