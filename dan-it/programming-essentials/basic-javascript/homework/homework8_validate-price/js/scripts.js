'use strict';
const wrapper = document.createElement('div');
document.body.prepend(wrapper);
const inputPrice = document.createElement('input');
inputPrice.style.display = 'block';
inputPrice.setAttribute('type', 'number');
inputPrice.setAttribute('placeholder', 'price');
wrapper.append(inputPrice);

const attention = document.createElement('span');
const closeButton = document.createElement('button');
const spanPrice = document.createElement('span');

function removeElements() {
    attention.remove();
    spanPrice.remove();
    closeButton.remove();
}
function wipe() {
    removeElements();
    inputPrice.value = '';
}

inputPrice.addEventListener('focus', focusBorder);
function focusBorder() {
    inputPrice.style.outline = 'none';
    inputPrice.style.border = '2px solid green';
    removeElements();
}

inputPrice.addEventListener('blur', blurBorder);
function blurBorder() {
    if (inputPrice.value == '') {
        attention.innerText = 'Enter the price';
        wrapper.append(attention);
        inputPrice.style.border = '';
    } else if (inputPrice.value < 0 || inputPrice.value === '') {
        attention.innerText = 'Please enter correct price';
        inputPrice.style.border = '2px solid red';
        wrapper.append(attention);
    } else {
        spanPrice.innerText = `Текущая цена: ${inputPrice.value}`;
        wrapper.prepend(closeButton);
        wrapper.prepend(spanPrice);
        closeButton.innerText = 'X';
        closeButton.style.marginLeft = '10px';
        closeButton.style.marginTop = '2px';
        closeButton.style.color = 'red';
        closeButton.addEventListener('click', wipe);
    }
}
