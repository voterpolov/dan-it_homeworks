const images = document.querySelectorAll('.image-to-show');
const imgWrapper = document.querySelector('.images-wrapper');
const pauseButton = document.createElement('button');
const resumeButton = document.createElement('button');
pauseButton.innerText = 'Pause cycle';
resumeButton.innerText = 'Resume cycle';
imgWrapper.append(pauseButton);
imgWrapper.append(resumeButton);
let sliderTimer;
let index = 0;
doWork();

function showSlide(index) {
    for (let i = 0; i < images.length; i++) {
        images[i].classList.add('remove');
    }
    images[index].classList.remove('remove');
}

function nextIndex() {
    if (index >= images.length - 1) {
        index = 0;
    } else {
        index++;
    }
}

function doWork() {
    showSlide(index);
    sliderTimer = setTimeout(() => {
        nextIndex();
        doWork();
    }, 1000);
}

function shutdown() {
    if (sliderTimer) {
        clearInterval(sliderTimer);
        sliderTimer = undefined;
    }
}

pauseButton.addEventListener('click', shutdown);
resumeButton.addEventListener('click', () => {
    shutdown();
    doWork();
});
