const questions = new Randomizer({
   items: [
      //add you questions here
   ],
   storageName: "questions",
   title: "Questions randomizer",
   parent: document.querySelector(".container"),
});

const students = new Randomizer({
   items: [
      "Белкін Євгеній",
      "Волотовський Василь",
      "Дорощук Лариса",
      "Забозін Микита",
      "Іванець Денис",
      "Коваленко Євгеній",
      "Лапченко Оксана",
      "Лук‘янчук Сергій",
      "Львович Олександр",
      "Малога Маргарита",
      "Мельник Сергій",
      "Мусієнко Артем",
      "Павлик Дарій",
      "Павлик Олександр",
      "Андрей Пархомец",
      "Пащенко Катерина",
      "Подосьолов Олександр",
      "Прус Владислав",
      "Реут Ксенія",
      "Соверченко Дмитро",
      "Суй Антоній",
      "Ухань Максим",
      "Щекін Олег",
      "Бойко Юлія",
      "Савченко Олена",
      "Фазлєтдінов Руслан",
   ],
   storageName: "students",
   title: "Students randomizer",
   parent: document.querySelector(".container"),
});
