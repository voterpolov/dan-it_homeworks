const browserSync = require('browser-sync').create();

function serv() {
  browserSync.init({
    server: {
      baseDir: './',
    },
    browser: 'firefox',
    injectChanges: false,
    logLevel: 'debug',
    files: [
      {
        match: ['**/*.scss', '**/*.js', '**/*.html'],
        fn: function (event, file) {
          this.reload();
        },
      },
    ],
  });
}

exports.serv = serv;
exports.bs = browserSync;
